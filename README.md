# The Lazy engineer's guide to running your Go web application to AWS Lambda

Sample code to go alongside the article [_The Lazy engineer's guide to running your Go web application to AWS Lambda_](https://www.jvt.me/posts/2023/05/21/lazy-go-lambda/).

Licensed under Apache-2.0.
