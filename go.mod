module gitlab.com/tanna.dev/lazy-port-lambda-go

go 1.20

require github.com/aws/aws-lambda-go v1.19.1

require (
	github.com/awslabs/aws-lambda-go-api-proxy v0.14.0 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
