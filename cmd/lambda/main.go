package main

import (
	"gitlab.com/tanna.dev/lazy-port-lambda-go/internal/httpserver"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/awslabs/aws-lambda-go-api-proxy/httpadapter"
)

func main() {
	mux := httpserver.NewServer()

	// note that this is using V2 API Gateway proxy events, which may need to be configured
	lambda.Start(httpadapter.NewV2(mux).ProxyWithContext)
}
