package main

import (
	"log"
	"net/http"

	"gitlab.com/tanna.dev/lazy-port-lambda-go/internal/httpserver"
)

func main() {
	mux := httpserver.NewServer()

	log.Println(http.ListenAndServe(":3000", mux))
}
